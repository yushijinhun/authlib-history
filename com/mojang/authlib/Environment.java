package com.mojang.authlib;

public record Environment(String accountsHost, String sessionHost, String servicesHost, String name) {
}
